# DID:PSQR Identifiers, Documents and Hosting

## PSQR Identifier Examples:
|                                   |                                     |
| --------------------------------- | ----------------------------------- |
|**Domain level Identifier:**       | `did:psqr:example.com`              |
|**User Profile level Identifier:** | `did:psqr:example.com/user`         |
|**Public Key Reference:**          | `did:psqr:example.com/user#publish` |

**NOTE:** Identifiers never end with a forward slash. Similarly, There won't be a forward slash before a Public Key Reference *(e.g: #publish)*

## PSQR publicKeys considerations:
* PSQR exclusively uses **ECDSA-P384** for signature generation.
* Each element of the publicKey array is a JWK containing an ES384 (P-384) Key.
    * Each ES384 Key is represented by an array with the following elements:
        * `"kty": "EC"`                             *(Key Type: Elliptic Curve)*
        * `"crv": "P-384"`                          *(Curve)*
        * `"x": "1beexcFBjN.............oH5Pz3v0Y"` *(X Coordinate)*
        * `"y": "-jMdfTnpnC.............1kALR9kW4"` *(Y Coordinate)*
    * Additional elements may be included such as:
        * `"kid": "did:psqr:example.com/user#publish"` *(PSQR Key ID)*
        * `"alg": "ES384"`                          *(Algorithm)*
    * **NOTE:** The Private key Element `"d"` must NEVER be included in the 

## Sample PSQR Data Model:
* **"id"** *DID Key ID. e.g: `did:psqr:example.com/user`*
* **"psqr"**
    * **"publicIdentity"**
        * **"name"**
        * **"description"**
    * **"publicKeys"**
        * **"kty"**
        * **"crv"**
        * **"alg"** 
        * **"x"** *e.g: `1beexcFBjN.............oH5Pz3v0Y`*
        * **"y"** *e.g: `-jMdfTnpnC.............1kALR9kW4`*
        * **"kid"** *e.g: `did:psqr:example.com/user#publish`*
    * **"permissions"**
        * **"kid"** *DID Identifier e.g: `did:psqr:example.com/user#publish`*
        * **"grant"**
            * **"publish"** *Indicates This key publishes content.*
            * **"provenance"** *Indicates This key signs content for future reader validation.*
    * **"updated**" *epoch in milliseconds*
 
JSON Example:
```JSON
{
  "@context":[
    "https://www.w3.org/ns/did/v1",
    "https://vpsqr.com/ns/did-psqr/v1"
  ],
  "id":"did:psqr:example.com/user",
  "psqr":{
    "publicIdentity":{
      "name":"Sample DID PSQR Identity"
    },
    "publicKeys":[
      {
        "kty":"EC",
        "crv":"P-384",
        "x":"1beexcF9kW2ZbE5QYN6Q5mqeOI3woi33SbP3_81kALR9kW2ZbE5QJ4yCcjE0OcXWy",
        "y":"-jnmfwoq8436tyhNWOET+nwoeiguiwqetyih1rj018923517yhNzucC0aezTdxaUr",
        "alg":"ES384",
        "kid":"did:psqr:example.com/user#publish"
      }
    ],
    "permissions":[
      {
        "grant":[
          "publish",
          "provenance"
        ],
        "kid":"did:psqr:example.com/user#publish"
      }
    ],
    "updated":1683304991117
  }
}
```

## DID:PSQR Resolution:

| Identifier                             | Resolved lookup Address              |
| -------------------------------------- | ------------------------------------ |
|**`did:psqr:example.com`**              | https://example.com/.well-known/psqr |
|**`did:psqr:example.com/user`**         | https://example.com/user             |
|**`did:psqr:example.com/user#publish`** | https://example.com/user             |

## PSQR Document Resolution considerations:
* Resolver generates the HTTPS URL as follows:
    * `did:psqr:example.com` ==> `https://example.com/.well-known/psqr`
    * `did:psqr:example.com/user` ==> `https://example.com/user`
    * `did:psqr:example.com/user#publish` ==> `https://example.com/user`
    * Resolver requests JSON Content with an `Accept` header, and a Content Type of either: `application/json`, `application/did+json`, or `application/json, application/did+json`
    * Resolver client must NOT follow HTTP Redirects (3xx)

## PSQR Document Hosting considerations:
* The Document host must return the DID document with a JSON Content Type
* The Host expects either of these headers:
    * `Accept: "application/json"`
    * `Accept: "application/did+json"`
    * `Accept: "application/json, application/did+json"`
* See below for sample configurations for **Apache**, and **Nginx**

## Web server configuration
Assuming your configured Document Root is `/var/www/html`, Using the following Apache configuration:
* You may host the Domain DID Document as `/var/www/html/.well-known/psqr.json`
* Similarly, you can host a specific Document (e.g: example) under `/var/www/html/example/psqr.json`

### Nginx Configuration Example:
```
location ~ ^/[a-zA-Z0-9]+(#.*)?$ {
  if ($http_accept ~* "application/json|application/did\+json") {
      rewrite ^/(.*)$ /$1/psqr.json break;
  }
}

location = /.well-known/psqr {
  if ($http_accept ~* "application/json|application/did\+json") {
    rewrite ^/(.*)$ /$1.json break;
  }
}

```
### Apache Configuration Example:

```
<IfModule mod_rewrite.c>
  RewriteEngine On

  RewriteCond %{HTTP:Accept} application/json [OR]
  RewriteCond %{HTTP:Accept} application/did\+json
  RewriteCond %{REQUEST_URI} ^/[a-zA-Z0-9]+(#.*)?$
  RewriteRule ^ %{REQUEST_URI}/psqr.json [L]

  RewriteCond %{HTTP:Accept} application/json [OR]
  RewriteCond %{HTTP:Accept} application/did\+json
  RewriteCond %{REQUEST_URI} ^/.well-known/psqr$
  RewriteRule ^ %{REQUEST_URI}.json [L]
</IfModule>
```