#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# ZWI Files Signature operations component.
#
# Code Dependencies (debian): apt update && apt install -y python3 python3-cryptography python3-jwt python3-requests openssl unzip
#
# Curve-P 384 ES384 Key Generation: openssl ecparam -name secp384r1 -genkey -noout -out es384.pem 
#   Optional: You can extract the public key from the generated PEM: openssl ec -in es384.pem -pubout -out es384.pub
#
# Config file sample format:
#   [zwi_signature]
#   identityName = Project DARA
#   identityAddress = info@dara.global - https://gutenberg.dara.global
#   kid = did:psqr:did.dara.global/gutenberg#publish
#
# @version 2.0, 20230520
# BGNLouie 
#

import argparse, json, hashlib, jwt, base64, shutil, sys, binascii, zipfile, os, subprocess, urllib.parse, requests, configparser, datetime
from collections import OrderedDict
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat, load_pem_private_key, load_pem_public_key
from cryptography.hazmat.primitives.asymmetric import ec, rsa

class AC:
    BL = '\033[94m'
    RD = '\033[91m'
    GN = '\033[92m'
    RST = '\033[0m'
    GNCHK = GN + u'\u2713' + RST
    FAIL = RD + 'x' + RST
    ERR = RD + 'Error:' + RST
    DBG = BL + 'D:' + RST

error_messages = []

# Buffers Error messages to show at the end of the process
def deferred_error_handler(e):
    global error_messages
    error_messages.append(str(e))

# Converts PEM type key to Coordinates
def pem2jwk(public_key):
    try:
        if isinstance(public_key, ec.EllipticCurvePublicKey):
            numbers = public_key.public_numbers()
            jwk = {
                "kty": "EC",
                "crv": "P-384" if numbers.curve.name == 'secp384r1' else None,
                "x": base64.urlsafe_b64encode(numbers.x.to_bytes(48, byteorder='big')).decode('utf-8'),
                "y": base64.urlsafe_b64encode(numbers.y.to_bytes(48, byteorder='big')).decode('utf-8')
            }
        else: jwk = None
    except: jwk = None

    return jwk

# Converts Key Coordinates to a PEM Public Key
def jwk2pem(jwk):
    try:
        # RS256 JWK
        if jwk.get("kty") == "RSA":
            public_numbers = rsa.RSAPublicNumbers(
                e=int.from_bytes(base64.urlsafe_b64decode(jwk.get("e") + "==="), byteorder="big"),
                n=int.from_bytes(base64.urlsafe_b64decode(jwk.get("n") + "==="), byteorder="big"),
            )
        # ES384 JWK
        elif jwk.get("kty") == "EC" and jwk.get("crv") == "P-384":
            curve = ec.SECP384R1()
            x = int.from_bytes(base64.urlsafe_b64decode(jwk.get("x") + "==="), byteorder="big")
            y = int.from_bytes(base64.urlsafe_b64decode(jwk.get("y") + "==="), byteorder="big")
            public_numbers = ec.EllipticCurvePublicNumbers(x=x, y=y, curve=curve)
        else: print(f"{AC.ERR} Unsupported JWK algorithm."); return False
    except:
        print(f"{AC.ERR} Failed to process JWK.")
        return False

    return public_numbers.public_key().public_bytes(Encoding.PEM, PublicFormat.SubjectPublicKeyInfo)

# ZWI Signature Handler
def sign_zwi(zwi_file, private_key_file, identity_data, force_overwrite=None):
    with open(private_key_file, "rb") as key_file:
        # Ensure pem File represents an ES384 private key
        private_key = load_pem_private_key(key_file.read(), None)
        if isinstance(private_key, ec.EllipticCurvePrivateKey) and (private_key.curve.name == 'secp384r1'): 
            key_alg="ES384"
            print(f" : Using Private key file = {AC.BL}{private_key_file}{AC.RST}")
            print(f" : Using algorithm = {AC.BL}{key_alg}{AC.RST}")
        else: print(f"{AC.ERR} Only Curve-P 384 (ES384) key pairs are supported."); return False

        # Generate the JWK Header from Private Key
        public_key = private_key.public_key()
        jwk = pem2jwk(public_key)
        if jwk is None: print(f"{AC.ERR} Failed to create JWK Header."); return False
        else: print(f" : Generated {AC.BL}JWK Header{AC.RST} ")

        public_key_pem = public_key.public_bytes( encoding=Encoding.PEM, format=PublicFormat.SubjectPublicKeyInfo ).decode('utf-8')

        # Initialize signature.json file, and handle force overwrite option
        with zipfile.ZipFile(zwi_file, 'r') as z:
            if "signature.json" in z.namelist() and force_overwrite: remove_signature(zwi_file, True)
            elif "signature.json" in z.namelist() and not force_overwrite: print(f"{AC.RD} == signature.json already exists! =={AC.RST}"); return False

        with zipfile.ZipFile(zwi_file, 'a') as z:
            # Generate the Timestamp for "Updated" and "iat" elements
            now = datetime.datetime.now(datetime.timezone.utc)
            dt_string = now.strftime("%Y-%m-%dT%H:%M:%S.%f")
            print(f" : Time when it was signed = {AC.BL}{dt_string}{AC.RST}")

            # Generate SHA1 Hash for metadata.json
            metadatahash = hashlib.sha1(z.open("metadata.json").read()).hexdigest()
            print(f" : Calculated sha1sum of metadata.json = {AC.BL}{metadatahash}{AC.RST}")

            # Generate JWT Payload (body)
            jwt_payload = {
                "iss": identity_data.get('identityName'),
                "address": identity_data.get('identityAddress'),
                "iat": int(now.timestamp()),
                "metadata": metadatahash
            }

            # Generate SHA1 Hash for media.json if found
            if "media.json" in z.namelist(): 
                mediahash = hashlib.sha1(z.open("media.json").read()).hexdigest()
                jwt_payload["media"] = mediahash
                print(f" : Calculated sha1sum of media.json = {AC.BL}{mediahash}{AC.RST}")
            
            # Generate JWT Header with JWK
            jwt_header = {
                "typ": "JWT",
                "alg": key_alg,
                "jwk": jwk,
                **({"kid": identity_data.get('kid')} if identity_data.get('kid') else {})
            }

            # Compile JWT Token to be injected in Signature.json
            jwt_token = jwt.encode(jwt_payload, private_key, algorithm=key_alg, headers=jwt_header)
            if isinstance(jwt_token, bytes): jwt_token = jwt_token.decode('utf-8') # utf-8 serialization workaround for an issue reported by Sergei
            # Compile signature.json
            signature_data = OrderedDict([
                ('identityName', identity_data.get('identityName')),
                ('identityAddress', identity_data.get('identityAddress')),
                ('token', jwt_token),
                ('alg', key_alg),
                ('updated', dt_string)
            ])

            # Inject signature.json and save ZWI File
            with z.open("signature.json", "w") as f:
                f.write(json.dumps(signature_data, indent=4).encode('utf-8'))
                print(f"{AC.GN} == ZWI Signed Successfully! =={AC.RST}")

# DID Resolver and Validator, Fetches Key from a validated DID:PSQR Document, or a Public Key (RS256) from a URL (Backwards-compatibility)
def url2key(url, key_alg):
    url = url.lower()
    did_keys = {}
    jwk = ''
    key_alg_map = { "ES384": ("EC", "P-384"), "RS256": ("RSA", "RS256") }
    requests_json_header = { 'Accept': 'application/json' }

    # Verify Key Algorithm is Supported
    try: did_kty, did_alg = key_alg_map[key_alg]
    except: deferred_error_handler(f"{AC.ERR} Unsupported Algorithm."); return False

    # DID:PSQR Handler
    if url.startswith("did:psqr:"):

        did_url, kid = url.split("#", 1) if "#" in url else (url, None)
        components = did_url.split(":"); optional_path = "/".join(components[3:])
        did_url = f"https://{urllib.parse.unquote(components[2])}"
        did_url += f"/{optional_path}" if optional_path else ""
        did_url += "/.well-known/psqr" if did_url.count("/") == 2 else ""
        if args.debug: print(f"{AC.DBG} Fetching DID:PSQR Document from: {AC.BL}{did_url}{AC.RST}")

        # Fetch and Parse DID Document
        try:
            response = requests.get(did_url, headers=requests_json_header)
            response.raise_for_status()
            did_document = response.json()
            if all(key not in did_document for key in ['psqr', 'verificationMethod', 'authentication']):
                deferred_error_handler(f"{AC.FAIL}\n{AC.ERR} Failed to parse DID Document."); return False
        except requests.exceptions.HTTPError as e: 
            deferred_error_handler(f"{AC.ERR} DID Document Server returned an HTTP Error Response."); return False
        except requests.exceptions.RequestException as e: 
            deferred_error_handler(f"{AC.ERR} Unable to connect to DID Document Server."); return False
        except: 
            deferred_error_handler(f"{AC.ERR} Invalid DID Document."); return False

        try:
            # Find "Authentication" element and verify supplied KID is valid
            if 'authentication' in did_document:
                for auth_id in did_document.get('authentication'):
                    if auth_id == url:
                        for vm in did_document.get('verificationMethod', []):
                            if vm.get('id') == auth_id or vm.get('kid') == auth_id: jwk = vm.get('publicKeyJwk', {})

            # Find "psqr->permissions" element and verify supplied KID is valid
            elif 'psqr' in did_document and 'permissions' in did_document.get('psqr'):
                for perm in did_document.get('psqr', {}).get('permissions'):
                    if 'grant' in perm and 'publish' in perm.get('grant') and 'kid' in perm and perm.get('kid') == url:
                        for pk in did_document.get('psqr', {}).get('publicKeys', []):
                            if pk.get('kid') == perm.get('kid') or pk.get('id') == perm.get('kid'): jwk = pk

            # Create a Public Key from Key coordinates
            if not jwk or jwk =='': deferred_error_handler(f"{AC.ERR} No valid matching key found in DID Document!!"); return False
            else: public_key_pem=jwk2pem(jwk)
        except: deferred_error_handler(f"{AC.ERR} Failed to parse DID Document."); return False

    # Plain text Public Key URL Handler
    elif url.startswith("http://") or url.startswith("https://"):
        try:
            response=requests.get(url)
            response.raise_for_status()
            try:
                public_key_pem = response.text.strip().encode('utf-8')
                public_key_test = load_pem_public_key(public_key_pem)
                if ((isinstance(public_key_pem, rsa.RSAPublicKey)) and (key_alg != "RS256")) or ((isinstance(public_key_pem, ec.EllipticCurvePublicKey)) and (key_alg != "ES384")):
                    deferred_error_handler(f"{AC.ERR} Key type mismatch."); return False
            except (ValueError, Exception, Exception) as e:
                deferred_error_handler(f"{AC.ERR} ee", {e.args[0]}); return False
        except requests.exceptions.HTTPError as e: 
            deferred_error_handler(f"{AC.ERR} Public Key Server returned an HTTP Error Response."); return False
        except requests.exceptions.RequestException as e: 
            deferred_error_handler(f"{AC.ERR} Unable to connect to Public Key Server Failed."); return False
        except: 
            deferred_error_handler(f"{AC.ERR} Failed fetching Remote key."); return False
    else: 
        deferred_error_handler(f"{AC.ERR} Invalid URL format (URLs must start with did:psqr, http:// or http://"); return False

    return public_key_pem

# ZWI Validation Handler
def validate_zwi(zwi_file, url=None, fast_validation=False):
    jwk_header = ''
    metadata_content_check_failed = False
    media_content_check_failed = False

    # Open ZWI File for Reading and Confirm signature.json exists
    with zipfile.ZipFile(zwi_file, 'r') as z:
        if 'signature.json' not in z.namelist(): deferred_error_handler(f"{AC.RD} == No signature.json found in ZWI file. =={AC.RST}"); return False
        with z.open('signature.json', 'r') as f:
            # Read signature JSON and JWT
            signature_data = json.loads(f.read())
            if args.debug: print(f"{AC.DBG} Contents of signature.json: {json.dumps(signature_data, indent=2)}")
            jwt_token = signature_data.get("token")
            if not jwt_token: deferred_error_handler(f"{AC.ERR} No JWT token found in signature.json."); return False

        # Get Signature Algorithm from the main data-set
        key_alg = signature_data.get("alg")
        print(f" : Using algorithm = {AC.BL}{key_alg}{AC.RST} ", end="")
        if not key_alg: 
            print(f"{AC.FAIL}")
            deferred_error_handler(f"{AC.ERR} No key algorithm found in signature.json."); return False
        elif not key_alg in ('RS256', 'ES384'): 
            print(f"{AC.FAIL}")
            deferred_error_handler(f"{AC.ERR} Unsupported Algorithm specified in signature.json."); return False
        else: print(f"{AC.GNCHK}")

        if url: # Fetch Public Key from user supplied URL/DID
            public_key_pem = url2key(url, key_alg)
            print(f" : Using User supplied {AC.BL}{url}{AC.RST} for Integrity Validation ", end="")
            if not public_key_pem: print(f"{AC.FAIL}"); return False
        else:
            try:
                jwk_header = jwt.get_unverified_header(jwt_token).get('jwk')
            except:
                print(f"{AC.FAIL}")
                deferred_error_handler(f"{AC.ERR} Invalid Token.")
                return False

            publickey_signature = signature_data.get("publicKey")
            if jwk_header: # Use JWK Header from the decoded Token
                print(f" : Using {AC.BL}JWK from embedded token{AC.RST} for Integrity Validation ", end="")
                public_key_pem = jwk2pem(jwk_header)
            elif publickey_signature and not jwk_header: # Use Plain Public Key from signature.json (backwards compatibility)
                print(f" : Using {AC.BL}Embedded public key from signature.json{AC.RST} for Integrity Validation ", end="")
                public_key_pem = signature_data.get("publicKey").encode('utf-8')
            else:
                print(f"{AC.FAIL}")
                deferred_error_handler(f"{AC.ERR} No public key found.")
                return False

        # Process Public Key
        try:
            public_key = load_pem_public_key(public_key_pem)
            decoded_jwt = jwt.decode(jwt_token, public_key, algorithms=key_alg)
            print(f"{AC.GNCHK}")
            if args.debug: print(f"{AC.DBG} Decoded Public Key: {AC.BL}{public_key_pem}{AC.RST}")
        except:
            print(f"{AC.FAIL}")
            if args.debug: print(f"{AC.DBG} Decoded Public Key: {AC.BL}{public_key_pem}{AC.RST}")
            deferred_error_handler(f"{AC.ERR} Public Key Error.")
            return False

        # Verify PSQR Key
        kid_header = jwt.get_unverified_header(jwt_token).get('kid')
        if kid_header and not url:
            try: 
                public_key_pem = url2key(kid_header, key_alg)
                print(f" : Validating Key ID {AC.BL}{kid_header}{AC.RST} ", end='')
                if public_key_pem and public_key_pem !='':
                    public_key = load_pem_public_key(public_key_pem)
                    psqr_token = jwt.decode(jwt_token, public_key, algorithms=key_alg)
                    if psqr_token == decoded_jwt:
                        print(f"{AC.GNCHK}")
                    else:
                        print(f"{AC.FAIL}")
                        deferred_error_handler(f"{AC.ERR} DID:PSQR Validation Failed.");
                else: print(f"{AC.FAIL}")
            except:
                print(f"{AC.FAIL}")
                deferred_error_handler(f"{AC.ERR} Invalid DID:PSQR Key.");

        # Extract Signed Organization name/Address and Signature Date
        if not decoded_jwt.get("authority"):
            decoded_jwt["authority"] = {}
            if decoded_jwt.get('iss'): decoded_jwt["authority"]["identityName"] = decoded_jwt.get('iss')
            if decoded_jwt.get('address'): decoded_jwt["authority"]["identityAddress"] = decoded_jwt.get('address')
            if decoded_jwt.get('iat'): decoded_jwt["authority"]["updated"] = datetime.datetime.utcfromtimestamp(decoded_jwt.get('iat')).strftime("%Y-%m-%dT%H:%M:%S.%f")

        # Verify Organization name (issuer) in the signed token matches that in signature.json
        print(f" : Signing organization = {AC.BL}{signature_data.get('identityName')} ", end='')
        if signature_data.get("identityName") == decoded_jwt.get('authority', {}).get('identityName'): 
            print(f" {AC.GNCHK}")
        else: deferred_error_handler(f"{AC.ERR} identityName mismatch: {AC.BL}{signature_data.get('identityName')} - {AC.RD}{decoded_jwt.get('authority', {}).get('identityName')}"); print(f" {AC.FAIL}")

        # Verify Organization address in the signed token matches that in signature.json
        print(f" : Address of Organization = {AC.BL}{signature_data.get('identityAddress')} ", end='')
        if signature_data.get("identityAddress") == decoded_jwt.get("authority", {}).get("identityAddress"): print(f" {AC.GNCHK}")
        else: deferred_error_handler(f"{AC.ERR} identityAddress mismatch: {AC.BL}{signature_data.get('identityAddress')} - {AC.RD}{decoded_jwt.get('authority', {}).get('identityAddress')}"); print(f" {AC.FAIL}")

        # Verify Token issue timestamp matches that in signature.json
        print(f" : Time when it was signed = {AC.BL}{signature_data.get('updated')} ", end='')
        signature_updated = signature_data.get("updated")
        decoded_updated = decoded_jwt.get("authority", {}).get("updated")
        error_sig_updated = f"{AC.ERR} Signing date mismatch: {AC.BL}{decoded_updated} - {AC.RD}{signature_updated}{AC.RST}"
        if signature_updated and decoded_updated:
            if signature_updated[:-7] + '.000000' == decoded_updated[:-7] + '.000000': print(f"{AC.GNCHK}")
            else: deferred_error_handler(error_sig_updated); print(f" {AC.FAIL}")
        else: deferred_error_handler(error_sig_updated); print(f" {AC.FAIL}")

        # metadata.json Verification
        if 'metadata.json' in z.namelist():
            if not fast_validation: # Verifies metadata.json sha1sum as well as the sums of the files listed under 'Content'
                with z.open('metadata.json', 'r') as metadatajsonfile:
                    # Read signature JSON and JWT
                    metadata_json = json.loads(metadatajsonfile.read())
                    metadata_json_contents = metadata_json.get('Content')
                    if metadata_json_contents:
                        if args.debug: print(f"\n{AC.DBG} Verifying metadata.json Contents signatures:")
                        for metadata_content_file in metadata_json_contents:
                            # Handles missing 'Content' Files
                            if metadata_content_file not in z.namelist():
                                metadata_content_check_failed = True
                                deferred_error_handler(f"{AC.ERR} {metadata_content_file}: {AC.RD}Missing{AC.RST} from ZWI Archive")
                                if args.debug: print(f"{AC.DBG} sha1sum - {AC.RD}{metadata_content_file}{AC.RST}: File Not Found! {AC.FAIL}")
                            else:
                                try:
                                    # Calculate SHA1SUM of 'Content' File and compare to Claimed checksum in metadata.json
                                    metadata_content_file_sha1 = hashlib.sha1(z.open(metadata_content_file).read()).hexdigest()
                                    metadata_json_file_sha1 = metadata_json_contents.get(metadata_content_file)
                                    if args.debug: print(f"{AC.DBG} sha1sum - {metadata_content_file}: {metadata_content_file_sha1} vs {metadata_json_file_sha1} ", end='')
                                    if metadata_content_file_sha1 == metadata_json_file_sha1:
                                        if args.debug: print(f"{AC.GNCHK}")
                                    else:
                                        metadata_content_check_failed = True
                                        deferred_error_handler(f"{AC.ERR} {metadata_content_file} sha1sum {AC.RD}Mismatch{AC.RST}!")
                                        if args.debug: print(f"{AC.FAIL}")
                                except Exception as e:
                                    deferred_error_handler(f"{AC.ERR} Failed to calculate sha1sum of {metadata_content_file} - {e}")
                                    return False
                metadata_content_check_result=f"{AC.FAIL}" if metadata_content_check_failed else f"{AC.GNCHK}"
                print(f" : Calculated vs Claimed sha1sums of {AC.BL}metadata.json{AC.RST} 'Content' files {metadata_content_check_result}")
                if args.debug: print()

            # Verify the sha1sum of metadata.json file itself
            metadata_sha1 = hashlib.sha1(z.open("metadata.json").read()).hexdigest()
            if args.debug: print(f"{AC.DBG} metadata.json calculated sha1sum: {metadata_sha1} - signed sha1sum: {decoded_jwt.get('metadata')} ", end='')
            if decoded_jwt.get("metadata") != metadata_sha1:
                if args.debug: print(f"{AC.FAIL}")
                deferred_error_handler(f"{AC.ERR} Invalid metadata.json SHA1 hash: {AC.BL}{decoded_jwt.get('metadata')} - {AC.RD}{metadata_sha1}{AC.RST}")
                print(f" : Calculated vs Claimed sha1sum of  {AC.BL}metadata.json{AC.RST} file itself {AC.FAIL}")
            else: 
                if args.debug: print(f"{AC.GNCHK}")
                print(f" : Calculated vs Claimed sha1sum of  {AC.BL}metadata.json{AC.RST} file itself {AC.GNCHK}")
        # metadata.json does not Exist!!
        else:
            deferred_error_handler(f"{AC.RD} == No metadata.json found in ZWI file. =={AC.RST}"); return False

        # media.json Verification
        if 'media.json' in z.namelist():
            if not fast_validation: # Verifies media.json sha1sum as well as the sums of the files listed under 'Content'
                with z.open('media.json', 'r') as mediajsonfile:
                    # Read signature JSON and JWT
                    media_json_contents = json.loads(mediajsonfile.read())
                    if media_json_contents:
                        if args.debug: print(f"\n{AC.DBG} Verifying media.json Contents signatures:")
                        for media_content_file in media_json_contents:
                            # Handles missing media Files
                            if media_content_file not in z.namelist():
                                media_content_check_failed = True
                                deferred_error_handler(f"{AC.ERR} {media_content_file}: {AC.RD}Missing{AC.RST} from ZWI Archive")
                                if args.debug: print(f"{AC.DBG} sha1sum - {AC.RD}{media_content_file}{AC.RST}: File Not Found! {AC.FAIL}")
                            else:
                                try:
                                    # Calculate SHA1SUM of 'Content' File and compare to Claimed checksum in media.json
                                    media_content_file_sha1 = hashlib.sha1(z.open(media_content_file).read()).hexdigest()
                                    media_json_file_sha1 = media_json_contents.get(media_content_file)
                                    if args.debug: print(f"{AC.DBG} sha1sum - {media_content_file}: {media_content_file_sha1} vs {media_json_file_sha1} ", end='')
                                    if media_content_file_sha1 == media_json_file_sha1:
                                        if args.debug: print(f"{AC.GNCHK}")
                                    else:
                                        media_content_check_failed = True
                                        deferred_error_handler(f"{AC.ERR} {media_content_file} sha1sum {AC.RD}Mismatch{AC.RST}!")
                                        if args.debug: print(f"{AC.FAIL}")
                                except Exception as e:
                                    deferred_error_handler(f"{AC.ERR} Failed to calculate sha1sum of {media_content_file} - {e}")
                                    return False
                media_content_check_result=f"{AC.FAIL}" if media_content_check_failed else f"{AC.GNCHK}"
                print(f" : Calculated vs Claimed sha1sums of {AC.BL}media.json{AC.RST} Contents {media_content_check_result}")
                if args.debug: print()

            # Verify the sha1sum of media.json file itself
            media_sha1 = hashlib.sha1(z.open("media.json").read()).hexdigest()
            if args.debug: print(f"{AC.DBG} media.json calculated sha1sum: {media_sha1} - signed sha1sum: {decoded_jwt.get('media')} ", end='')
            if decoded_jwt.get("media") != media_sha1:
                if args.debug: print(f"{AC.FAIL}")
                deferred_error_handler(f"{AC.ERR} Invalid media.json SHA1 hash: {AC.BL}{decoded_jwt.get('media')} - {AC.RD}{media_sha1}{AC.RST}")
                print(f" : Calculated vs Claimed sha1sum of  {AC.BL}media.json{AC.RST} file itself {AC.FAIL}")
            else: 
                if args.debug: print(f"{AC.GNCHK}")
                print(f" : Calculated vs Claimed sha1sum of  {AC.BL}media.json{AC.RST} file itself {AC.GNCHK}")
    return True

# Signature Remover (Unsigner) handler
def remove_signature(zwi_path, force_overwrite=None):

    # Check if OS has the Zip package/command available, as it's much faster.
    def zip_command_exists():
        try: subprocess.run(['zip', '--version'], capture_output=True, text=True); return True
        except: return False

    if zip_command_exists():
        try:
            # Simply delete signature.json from the ZWI file
            result = subprocess.run(['zip', '-d', zwi_path, 'signature.json'], capture_output=True, text=True)
            # Catch if signature.json doesn't exist
            if result.returncode != 0: print(f"{AC.ERR} signature.json not found in ZWI file."); return False
        except Exception as e: print(f"{AC.ERR}", e); return False
    else:
        try:
            # 'zip' command doesn't exist, process the zip manually
            with zipfile.ZipFile(zwi_path, 'r') as zwi:
                if 'signature.json' not in zwi.namelist(): print(f"{AC.ERR} signature.json not found in ZWI file."); return False
            # ZWI Archive is decompressed in a temp path
            temp_zip_path = zwi_path + '.tmp'
            with zipfile.ZipFile(zwi_path, 'r') as zwi, zipfile.ZipFile(temp_zip_path, 'w') as temp_zip:
                # loop through the unzipped files till signature.json is found, then remove it
                for item in zwi.infolist():
                    if item.filename != 'signature.json':
                        data = zwi.read(item.filename)
                        temp_zip.writestr(item, data)
            # pack the archive back from the temp path
            shutil.move(temp_zip_path, zwi_path)
        except Exception as e: print(f"{AC.ERR}", e); return False
    if force_overwrite: print(f" : Removed existing signature"); return True
    else: print(f"{AC.GN} == Signature Removed successfully =={AC.RST}"); return True

def main():
    global args

    parser = argparse.ArgumentParser(description="Sign and Validate ZWI files.")
    parser.add_argument("-z", "--zwi", required=True, help="ZWI file")

    parser.add_argument("-s", "--sign", action="store_true", help="Sign the ZWI file.")
    parser.add_argument("-o", "--overwrite", action="store_true", help="Overwrite existing signature.json if found.")
    
    parser.add_argument("-v", "--validate", action="store_true", help="Validate the ZWI file Against Built in JWK Header if found, OR Embedded Public Key, Or Public Key from URL, or Key from DID URL")
    parser.add_argument("-f", "--fast", action="store_true", help="Perform fast validation which skips individual file sha1sums besides metadata.json and media.json")

    parser.add_argument("-r", "--remove", action="store_true", help="Remove Signature from the ZWI file")

    
    parser.add_argument("-k", "--key", help="Private key file (es384.pem)")
    parser.add_argument("-c", "--config", help="Config File with publisher's data: identityName, identityAddress, kid (DID:PSQR), etc")
    parser.add_argument("-u", "--url", help="DID:PSQR / Public Key URL")

    parser.add_argument("-d", "--debug", action="store_true", help="Show Debug output")

    # Ignore unknown command arguments
    args, unknown = parser.parse_known_args()

    if not args.sign and not args.validate and not args.remove: print(f"{AC.ERR} Missing operation parameter: Must either Sign (-s), Validate (-v), or Remove Signature (-r)."); sys.exit()
    if args.sign and args.validate: print(f"{AC.ERR} Cannot perform both signing and validation in a single run."); sys.exit()
    if args.zwi: print(f" : Input ZWI file = {AC.BL}{args.zwi}{AC.RST}")
    if not os.path.isfile(args.zwi) or not os.access(args.zwi, os.R_OK): print(f"{AC.ERR} Failed to read file"); sys.exit()

    if args.validate:
        validate_zwi(args.zwi, args.url, args.fast)
        if error_messages:
            for message in error_messages:
                print(message)
            print(f"{AC.RD} == !! Invalid Signature !! =={AC.RST}")
        elif args.fast:
            print(f"{AC.GN} == Quick Validation Successful =={AC.RST}")
        else:
            print(f"{AC.GN} == Signature is Valid =={AC.RST}")
            
    elif args.remove: remove_signature(args.zwi)

    elif args.sign:
        if not args.key: print(f"{AC.ERR} Missing private key (-k)"); sys.exit()
        if not args.config: print(f"{AC.ERR} Missing config file (-c)"); sys.exit()
        if args.config:
            config = configparser.ConfigParser()
            try: config.read(args.config)
            except: print(f"{AC.ERR} Failed to read {args.config}"); sys.exit()

            try:
                identityName = config.get('zwi_signature', 'identityName')
                if identityName == "": raise
                print(f" : Signing Organization = {AC.BL}{identityName}{AC.RST}")
            except: print(f"{AC.ERR} Failed to retrieve {AC.BL}'identityName'{AC.RST} from {AC.BL}{args.config}{AC.RST}"); sys.exit()
            
            try: 
                identityAddress = config.get('zwi_signature', 'identityAddress')
                if identityAddress == "": raise
                print(f" : Address of Organization = {AC.BL}{identityAddress}{AC.RST}")
            except: print(f"{AC.ERR} Failed to retrieve {AC.BL}'identityAddress'{AC.RST} from {AC.BL}{args.config}{AC.RST}"); sys.exit()

            try:
                kid = config.get('zwi_signature', 'kid')
                if kid == "": raise
                print(f" : PSQR kid = {AC.BL}{kid}{AC.RST}")
            except: print(f"{AC.ERR} Failed to retrieve {AC.BL}'kid'{AC.RST} from {AC.BL}{args.config}{AC.RST}"); sys.exit()
            if not kid.startswith("did:psqr:"): print(f"{AC.ERR} Invalid DID:PSQR Key"); sys.exit()

            identity_data = {"identityName": identityName, "identityAddress": identityAddress, "kid": kid}
            sign_zwi(args.zwi, args.key, identity_data, args.overwrite)
    else: print(f"{AC.ERR} Please choose to either sign (-s) or validate (-v) the ZWI file."); sys.exit()

if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(f'Error: {e}')
# Handle Imported Functions
else:
    def imp_validate_zwi(zwi_file, url=None, fast_validation=False, debug=False):
        global args
        args = argparse.Namespace(zwi=zwi_file, debug=debug, fast=fast_validation, url=url)
        if not os.path.isfile(args.zwi) or not os.access(args.zwi, os.R_OK): print(f"{AC.ERR} Failed to read file"); sys.exit()
        print(f" : Input ZWI file = {AC.BL}{args.zwi}{AC.RST}")
        validate_zwi(args.zwi, args.url, args.fast)
        if error_messages:
            for message in error_messages: print(message)
            print(f"{AC.RD} == !! Invalid Signature !! =={AC.RST}")
        elif args.fast: print(f"{AC.GN} == Quick Validation Successful =={AC.RST}")
        else: print(f"{AC.GN} == Signature is Valid =={AC.RST}")

    def imp_sign_zwi(zwi_file, private_key_file, config_file, identity_data, force_overwrite=False, debug=False):
        global args
        args = argparse.Namespace(zwi=zwi_file, key=private_key_file, config=config_file, overwrite=force_overwrite, debug=debug)
        if not os.path.isfile(args.zwi) or not os.access(args.zwi, os.R_OK): print(f"{AC.ERR} Failed to read file"); sys.exit()

        if not args.key: print(f"{AC.ERR} Missing private key"); sys.exit()
        if not args.config: print(f"{AC.ERR} Missing config file"); sys.exit()
        config = configparser.ConfigParser()
        try: config.read(args.config)
        except: print(f"{AC.ERR} Failed to read {args.config}"); sys.exit()

        try:
            identityName = config.get('zwi_signature', 'identityName')
            if identityName == "": raise
            print(f" : Signing Organization = {AC.BL}{identityName}{AC.RST}")
        except: print(f"{AC.ERR} Failed to retrieve {AC.BL}'identityName'{AC.RST} from {AC.BL}{args.config}{AC.RST}"); sys.exit()
        
        try: 
            identityAddress = config.get('zwi_signature', 'identityAddress')
            if identityAddress == "": raise
            print(f" : Address of Organization = {AC.BL}{identityAddress}{AC.RST}")
        except: print(f"{AC.ERR} Failed to retrieve {AC.BL}'identityAddress'{AC.RST} from {AC.BL}{args.config}{AC.RST}"); sys.exit()

        try:
            kid = config.get('zwi_signature', 'kid')
            if kid == "": raise
            print(f" : PSQR kid = {AC.BL}{kid}{AC.RST}")
        except: print(f"{AC.ERR} Failed to retrieve {AC.BL}'kid'{AC.RST} from {AC.BL}{args.config}{AC.RST}"); sys.exit()
        if not kid.startswith("did:psqr:"): print(f"{AC.ERR} Invalid DID:PSQR Key"); sys.exit()

        identity_data = {"identityName": identityName, "identityAddress": identityAddress, "kid": kid}
        print(f" : Input ZWI file = {AC.BL}{args.zwi}{AC.RST}")    
        sign_zwi(args.zwi, args.key, identity_data, args.overwrite)

    def imp_remove_signature(zwi_file, debug=False):
        global args
        args = argparse.Namespace(zwi=zwi_file, debug=debug)
        if not os.path.isfile(args.zwi) or not os.access(args.zwi, os.R_OK): print(f"{AC.ERR} Failed to read file"); sys.exit()
        print(f" : Input ZWI file = {AC.BL}{args.zwi}{AC.RST}")
        remove_signature(zwi_file)