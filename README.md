# ZWI Signature v2.0 - Updated 20230520
## _Signature operations component for ZWI files_

This code is used to sign, unsign and verify signatures of ZWI Files.
The signing component combines the issuer data along with sha1sums of all the metadata files within the ZWI such as metadata.json and media.json into a JWT payload 
along with a protected header containing the JWK which is used to validate the integrity of the signature, in place of a PEM format Public key.

ZWI Signature includes a URL and DID:PSQR Document resolver, allowing the verification to be performed against a Public Key stored as a PEM in a URL, or within a DID Document identified by DID:PSQR method.

Signing relies on Elliptic Curve P384 Signature algorithm for compliance with DID:PSQR standards.

The Verification component first verifies the integrity of the token (JWT) within signature.json, decoding it and validating it against the key (JWK) header.
If no JWK header was found, We can assume a PublicKey was included (Legacy Purposes), and the JWT is verified against the included PEM.
If a DID identifier was found in the JWT, it will be resolved, and the identifier will be matched against valid/active keys in the DID Document.
Finally the sha1sums in the JWT payload are compared to actual sha1sums calculated on the fly from within the ZWI archive.

## Key Features
- Sign a ZWI file using an ES384 private key
    - Creates a JWT with a protected JWK header for integrity checks.
    - JWT contains the issuer data along with the sha1sums for the metadata json files found in the ZWI
- Validates a ZWI file against the built in JWK header or PublicKey element in signature.json.
    - Confirms all sha1sums are correct
    - Verifies the signing key by performing a DID or URL resolution (kid), and decoding the token with the matched key.
    - Performs thorough verification of the sha1sum of every content file within the archive (if -f isn't specified for a fast validation)
- Remove a Signature from a ZWI file
- Retains backwards compatibility with older ZWI files signed by zwi_signature.py with RS256 algorithm and with a Public Key outside the token, where applicable.

# Installation and setup

Install the dependencies needed to run the python script and its component
Example given for Debian-11:

```sh
apt update && apt install -y python3 python3-cryptography python3-jwt python3-requests openssl unzip git
```

Download the script, and the sample config file.
```sh
git clone https://gitlab.com/BGNLouie/zwi-signature-v2/
```

Edit the sample config file and ensure the values for `identityName`, `identityAddress` and `kid` are correctly defined under the [zwi_signature] header

Create an Elliptic Curve-P 384 Key:
```sh
openssl ecparam -name secp384r1 -genkey -noout -out es384.pem
```

Optional: To retrieve the Public key from the private key you've just created:
```sh
openssl ec -in es384.pem -pubout -out es384.pub
```
## Usage examples

Signing a ZWI file:
```sh
python3 zwi_signature_v2.py -z test.zwi -k es384.pem -c signature.conf -s
```

Signing a ZWI file and Overwrite existing signatures if found:
```sh
python3 zwi_signature_v2.py -z test.zwi -k es384.pem -c signature.conf -s -o
```

Verification of ZWI File token signature, metadata and sha1sums of every Content and Media file within the archive:
```sh
python3 zwi_signature_v2.py -z test.zwi -v
```

Fast Validation of a ZWI file, through verification of the token signature, matadata, and sha1sums of metadata.json and media.json files:
```sh
python3 zwi_signature_v2.py -z test.zwi -v -f
```

Fast Validation of a ZWI file signature against a remote key URL/DID Document:
```sh
python3 zwi_signature_v2.py -z test.zwi -u did:psqr:encycloreader.org/ksf/#publish -v -f
```

Unsigning / Removing a ZWI file Signature:
```sh
python3 zwi_signature_v2.py -z test.zwi -r
```

For debugging/added verbosity, you may call any command with -d switch.

Batch Signing Example:
```
time ls zwi/*.zwi | while read zwifile; do python3 zwi_signature.py -z "$zwifile" -k es384.pem -c signature.conf -s -o; done
```

# ZWI Signature with Docker:

Clone the repo containing the code and config placeholder:
```sh
git clone https://gitlab.com/BGNLouie/zwi-signature-v2/
```

Ensure signature.conf and es384.pem are updated.
Note: The dockerized deployment would automatically generate a new Private key if one was not found during the build

Build the image:
```sh
docker build . -t zwi-signature
```
Note: The dockerized deployment has the config and key parameters built in for convenience, so there's no need for -c or -k parameters

Running the signature image for a one-time test:
`docker run -v ./<directory containing your ZWI files>:/zwi zwi_signature -z <ZWI Filename> [ command arguments ]`

## Testing with Docker:
These examples assume './myzwifiles' is the path to your ZWI files directory which contains test.zwi

Signing a ZWI file:
```sh
docker run -v ./myzwifiles:/zwi zwi_signature -z test.zwi -k es384.pem -c signature.conf -s
```

Signing a ZWI file and Overwrite existing signatures if found:
```sh
docker run -v ./myzwifiles:/zwi zwi_signature -z test.zwi -k es384.pem -c signature.conf -s -o
```

Verification of ZWI File token signature, metadata and sha1sums of every Content and Media file within the archive:
```sh
docker run -v ./myzwifiles:/zwi zwi_signature -z test.zwi -v
```

Fast Validation of a ZWI file, through verification of the token signature, matadata, and sha1sums of metadata.json and media.json files:
```sh
docker run -v ./myzwifiles:/zwi zwi_signature -z test.zwi -v -f
```

Fast Validation of a ZWI file signature against a remote key URL/DID Document:
```sh
docker run -v ./myzwifiles:/zwi zwi_signature -z test.zwi -u did:psqr:encycloreader.org/ksf/#publish -v -f
```

Unsigning / Removing a ZWI file Signature:
```sh
docker run -v ./myzwifiles:/zwi zwi_signature -z test.zwi -r
```

For debugging/added verbosity, you may call any command with -d switch.

Batch Signing is not recommended unless a container is deployed.. see below for more info

## Examples (Deploying a Container for production environment):
This examples assumes './myzwifiles' is the path to your ZWI files directory which contains test.zwi
Once the container is deployed, zwisign command simplifies the command parameters needed by implying -k es384.pem -c signature.conf parameters

Launch the docker container using the built image, and run a local BASH session:
```
docker run --entrypoint='' -v ./myzwifiles:/zwi -it zwi_signature bash
```
Navigate to your mounted ZWI Directory:
```
cd /zwi
```

Signing a ZWI file:
```sh
zwisign -z test.zwi -s
```

Signing a ZWI file and Overwrite existing signatures if found:
```sh
zwisign -z test.zwi -s -o
```

Verification of ZWI File token signature, metadata and sha1sums of every Content and Media file within the archive:
```sh
zwisign -z test.zwi -v
```

Fast Validation of a ZWI file, through verification of the token signature, matadata, and sha1sums of metadata.json and media.json files:
```sh
zwisign -z test.zwi -v -f
```

Fast Validation of a ZWI file signature against a remote key URL/DID Document:
```sh
zwisign -z test.zwi -u did:psqr:encycloreader.org/ksf/#publish -v -f
```

Unsigning / Removing a ZWI file Signature:
```sh
zwisign -z test.zwi -r
```

For debugging/added verbosity, you may call any command with -d switch.

Batch Signing Example using a Docker Container:
`docker run --entrypoint='' -v ./zwi:/zwi -it zwi_signature bash`
`cd /zwi`
`time ls *.zwi | while read zwifile; do zwisign -z "$zwifile" -s -o; done`

## Example Using zwi_signature_v2 as an external library:
Place `zwi_signature_v2.py` in the directory of your script, define the variables for `zwi_file` path, and in the case of signing, then variables for `private_key_file` and `config_file` are necessary.


Import zwi_signature_v2:
```py
from zwi_signature_v2 import *

zwi_file="files/test.zwi"
private_key_file="es384.pem"
config_file="signature.conf"
```

Signing a ZWI file:
```py
imp_sign_zwi(zwi_file, private_key_file, config_file, force_overwrite=False, debug=False)
```

Verifying a ZWI Signature:
```py
imp_validate_zwi(zwi_file, url=None, fast_validation=False, debug=True)
```

Removing a Signature:
```py
imp_remove_signature(zwi_file)
```